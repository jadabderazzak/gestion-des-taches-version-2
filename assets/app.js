/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import 'tw-elements';
import 'flowbite';


document.addEventListener('DOMContentLoaded', function () {
  var navbarToggle = document.querySelector('[data-collapse-toggle="navbar-default"]');
  var navbarMenu = document.getElementById('navbar-default');

  navbarToggle.addEventListener('click', function () {
    navbarMenu.classList.toggle('hidden');
  });
});

import {
  Ripple,
  initTE,
} from "tw-elements";

initTE({ Ripple });

/******************************** Gestion des etats d'une tâche Terminée ou Annulé */
// Normalement la fonction Finish cancel task est la même, mais je les ai séparés en cas d'ajout de fonctionnalité spécifiques pour chaque etat


// Tache terminée
window.finishTask = function finishTask(idTache) {

  // Parametre 'etat' recoie 1 pour Terminée et 0 pour Annulée
  $.ajax({
    type: "GET",
    url: "/tache/status/finish/" + idTache,

    success: function (data) {

      // Recuperation de l'id du badge et changement du TEXT ainsi que la couleur

     
      // Changer la couleur du badge depuis le beu au vert 
      var couleur = document.getElementById('couleur' + idTache);
      couleur.classList.remove('bg-blue-200');
      couleur.classList.add('bg-green-200');
       // Changer le text du badge depuis En cours à Terminée
      var text = document.getElementById('text' + idTache);
      text.textContent = data.toUpperCase(); 
     

      showToast("La tâche a bien été terminée!");

      $("#buttons" + idTache).fadeOut("slow");


    },
    error: function (jqXHR, textStatus, errorThrown) {


      alertify.error(jqXHR.responseJSON);

    }
  });



}

// Tache Annulée
window.cancelTask = function cancelTask(idTache) {

  // Parametre 'etat' recoie 1 pour Terminée et 0 pour Annulée
  $.ajax({
    type: "GET",
    url: "/tache/status/cancel/" + idTache,

    success: function (data) {

      // Changer le text du badge depuis En cours à Annulée
      var text = document.getElementById('text' + idTache);
      text.textContent = data.toUpperCase(); // Text
       // Changer la couleur du badge depuis le beu au rouge 
       var couleur = document.getElementById('couleur' + idTache);
       couleur.classList.remove('bg-blue-200');
       couleur.classList.add('bg-red-200');

      showToast("La tâche a bien été annulée!");

      $("#buttons" + idTache).fadeOut("slow");


    },
    error: function (jqXHR, textStatus, errorThrown) {


      alertify.error(jqXHR.responseJSON);

    }
  });



}

window.showToast = function showToast(message) {
  // Show the toast
  document.getElementById("myToast").classList.remove("hidden");
  // Set the message


  var element = document.getElementById("toastMSG");

  element.textContent = message; // Set the text content of the element

  setTimeout(function () {
    document.getElementById("myToast").classList.add("hidden");
  }, 5000);
}