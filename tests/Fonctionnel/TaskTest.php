<?php

namespace App\Tests\Fonctionnel;

use App\Entity\Etat;
use App\Entity\Tache;
use App\Repository\EtatRepository;
use App\Repository\TacheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TacheTest extends WebTestCase
{
    public function testTachePage(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->assertSelectorExists('p');
        $this->assertSelectorTextContains('p', 'Gestion des tâches');
    }

  


    /**
     * Tester si le filtre de la page Index retourne les résultat souhaité
     */
    
    public function testFilterIndex(): void
    {
        $client = static::createClient();

        /** @var UrlGeneratorInterface */
        $urlGeneratorInterface = $client->getContainer()->get('router');

        /** @var EntityManagerInterface */
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');

        /** @var TacheRepository */
        $TacheRepository = $entityManager->getRepository(Tache::class);

        /** @var EtatRepository */
        $etatRepository = $entityManager->getRepository(Etat::class);

        /** @var Tache */
        $Tache = $TacheRepository->findOneBy([]);

       

        /** @var Etat */
        $etat = $etatRepository->findOneBy([]);

        $crawler = $client->request(
            Request::METHOD_GET,
            $urlGeneratorInterface->generate('app_liste_taches')
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Récupérer les 3 premier caracteres du titre de la tache et de l'etat pour tester le resultat
        $searchs = [
            substr($Tache->getTitre(), 0, 3), // 'Tit'
            substr($etat->getLibelle(), 0, 3) // 'En '
        ];

      

        foreach ($searchs as $search) {
            $form = $crawler->filter('form[name=search]')->form([
                'search[mot]' => $search,
                'search[etat]' => 2  // Tache Terminée
            ]);


      
            $crawler = $client->submit($form);

            $this->assertResponseIsSuccessful();
            $this->assertResponseStatusCodeSame(Response::HTTP_OK);
            $this->assertRouteSame('app_liste_taches');

            $nbTaches = count($crawler->filter('div.card'));
            $taches = $crawler->filter('div.card');
            $count = 0;
      
            foreach ($taches as $index => $Tache) {
                $titre = $crawler->filter("div.card h6")->getNode($index);
                if (
                    str_contains($titre->textContent, $search)
                ) {
                    $TacheEtat = $crawler->filter('div.card div.badges')->getNode($index)->childNodes;

                    for ($i = 1; $i < $TacheEtat->count(); $i++) {
                        $Tacheetat = $TacheEtat->item($i);
                        $name = trim($Tacheetat->textContent);

                        if ($name === $etat->getName()) {
                            $count++;
                        }
                    }
                }
            }

            $this->assertEquals($nbTaches, $count);
        }

        
    }



}
