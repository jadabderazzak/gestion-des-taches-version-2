<?php

namespace App\Form;

use App\Entity\Etat;
use App\Model\RechercheDonnee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mot', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Recherche par Titre ...',
                    'class' => 'block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-r-lg border-l-gray-50 border-l-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-l-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:border-blue-500'
                ]
            ])
            ->add('etat', EntityType::class, [
                'required' => false,
                'class' => Etat::class,
                'placeholder' => 'Tous les etats',
                'choice_label' => 'libelle', 
          
            
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RechercheDonnee::class,
            'method' => 'GET',
            'csrf_protection' => false
            // Configure your form options here
        ]);
    }
}
