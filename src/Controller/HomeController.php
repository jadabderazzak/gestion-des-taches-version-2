<?php

namespace App\Controller;

use App\Entity\Tache;
use App\Form\TacheType;
use App\Form\SearchType;
use App\Model\RechercheDonnee;
use App\Repository\EtatRepository;
use App\Repository\TacheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_liste_taches')]
    public function index(TacheRepository $repoTache, PaginatorInterface $paginateur, Request $request, EtatRepository $repoEtat): Response
    {

        $rechercheDonnee = new RechercheDonnee();
        $form = $this->createForm(SearchType::class,$rechercheDonnee);

       


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            // Récuperation de l'id de l'etat depuis le request
            $etatId = $request->get('search')['etat'];
            // Recuperer l'entité Etat
            $etat = $etatId !== "" ? $repoEtat->findOneBy(['id'=> $etatId]) : null;

            $rechercheDonnee->page = $request->query->getInt('page', 1);
           // Paginer le resultat de findByTitle 
            $taches = $paginateur->paginate(
                $repoTache->findByTitle($rechercheDonnee, $etat),
                $request->query->get('page', 1),
                5
            );
            return $this->render('home/index.html.twig', [
                'taches' => $taches,
                'form' => $form->createView()
            ]);

        }else{


            $taches = $paginateur->paginate(
                $repoTache->queryPagination(),
                $request->query->get('page', 1),
                6
            );

        }

     
        return $this->render('home/index.html.twig', [
            'taches' => $taches,
            'form' => $form->createView()
        ]);
    }

    
}
