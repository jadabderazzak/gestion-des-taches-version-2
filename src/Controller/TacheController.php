<?php

namespace App\Controller;

use App\Entity\Tache;
use App\Form\TacheType;
use App\Repository\EtatRepository;
use App\Repository\TacheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TacheController extends AbstractController
{
    #[Route('/tache', name: 'app_tache')]
    public function index(): Response
    {
        return $this->render('tache/index.html.twig', [
            'controller_name' => 'TacheController',
        ]);
    }

    #[Route('/tache/new', name: 'app_tache_new')]
    public function newTache(Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
          $tache = new Tache();
    
        $form= $this->createForm(TacheType::class, $tache);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
                // Récuperer l'etat dont l'I est égale à 1
                $etat = $repoEtat->findOneBy([
                    'id' => 1
                ]);
                // Ajouter la date de création et l'etat en cours
                $tache->setEtat($etat);
       

            $manager->persist($tache);
            $manager->flush();
            
            $this->addFlash('success',"Tâche ajoutée avec succès.");
            // Faire une redirection vers la liste des tâches 
            return $this->redirectToRoute('app_liste_taches');
        }

        
        return $this->render('tache/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    
    #[Route('/tache/{id}/edit', name: 'app_tache_edit')]
    public function editTache(Tache $tache, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
         
     

        $form= $this->createForm(TacheType::class, $tache);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
               
            $manager->flush();

            $this->addFlash('success',"Tâche modifiée avec succès.");
            // Faire une redirection vers la liste des tâches 
            return $this->redirectToRoute('app_liste_taches');
        }

        
        return $this->render('tache/update.html.twig', [
            'form' => $form->createView(),
            
        ]);
    }

    #[Route('/tache/status/finish/{id}', name: 'app_finish_valid_Status')]
    public function finishTacheStatus(Tache $tache, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       $etat = $repoEtat->findOneBy([
        'id' => 2
       ]);
       $tache->setEtat($etat);
       $manager->flush();

        return $this->json($etat->getLibelle(), 200);
    }

    #[Route('/tache/status/cancel/{id}', name: 'app_change_cancel_Status')]
    public function cancelTaacheStatus(Tache $tache, Request $request, EntityManagerInterface $manager, EtatRepository $repoEtat): Response
    {
       
        $etat = $repoEtat->findOneBy([
            'id' => 3
           ]);
           $tache->setEtat($etat);
           $manager->flush();
        

        return $this->json($etat->getLibelle(), 200);
    }

    #[Route('/tables', name: 'app_tache_tables')]
    public function datatables(TacheRepository $repoTache): Response
    {

        
          $taches = $repoTache->findAll();
        return $this->render('tache/datatables.html.twig', [
            'taches' => $taches,
          
        ]);
    }
}
