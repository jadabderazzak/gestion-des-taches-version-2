<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use App\Entity\Tache;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $etat = $manager->getRepository(Etat::class)->find(1);
       
        

        for ($i = 0; $i < 50; $i++) {
            $task = new Tache();
            $task->setTitre("Titre ".$i)
                  ->setDescription("Description ".$i)
                  ->setEtat($etat);
            $manager->persist($task);

        }

        $manager->flush();
    }
}
